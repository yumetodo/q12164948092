﻿#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <limits>
#include <vector>
#include <tuple>
#include <string>
#include <sstream>
#include <locale>
#include <codecvt>
#include <type_traits>

using std::tuple;
using std::vector;

//std::basic_istream <- std::basic_ifstream

auto load_csv(std::istream& in)
    -> tuple<vector<double>, vector<double>, vector<double>>
{
    //x,y,z配列
    using std::get;
    tuple<vector<double>, vector<double>, vector<double>> re;
    auto& x = get<0>(re);
    auto& y = get<1>(re);
    auto& z = get<2>(re);

    for (std::string buf; getline(in, buf); ) {//CSVファイルを1行ずつに分割
        std::stringstream ss(buf);
        double tmp[3];
        for (auto&& t : tmp) {
            ss >> t;//文字列とカンマを分割
            ss.ignore((std::numeric_limits<std::streamsize>::max)(), ',');//カンマを飛ばして読み込み
        }
        x.push_back(tmp[0]);//x
        y.push_back(tmp[1]);//y
        z.push_back(tmp[2]);//z
    }
    return re;
}

int main() {
    using std::cout;
    using std::get;
    using std::endl;
    constexpr std::size_t i_MAX = 10;

    std::ifstream ifs("Book1.csv");//.xlsxは非対応
    using cvt = typename std::conditional<
        sizeof(wchar_t)==2, 
        std::codecvt_utf8_utf16<wchar_t, 0x10ffff, std::consume_header>,//Windows
        std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>//Linux
    >::type;
    ifs.imbue(std::locale(std::locale(""), new cvt()));
    if (!ifs) {
        cout << "Book1.csvフォルダーが見つかりません。\n";
        cout << "拡張子が.xlsxではありませんか？。\n";
        getchar();
        return -1;
    }
    const auto in = load_csv(ifs);
    if(i_MAX != get<0>(in).size()) return -1;
    //テキストに書き込み(1行目)
    std::ofstream outputfile("TextForSTL.txt", std::ios::out | std::ios::app);
    outputfile << R"(solid "TextForSTL")" << endl;

    for (std::size_t i = 0; i < i_MAX - 2; i++) {//(-2)はi_MAXのときi+1,i+2が無いため

        //頂点0の座標
        // long double a[3] = { get<0>(in)[i], get<1>(in)[i], get<2>(in)[i] };
        //頂点1の座標
        // long double b[3] = { get<0>(in)[i + 1], get<1>(in)[i + 1], get<2>(in)[i + 1] };
        //頂点2の座標
        // long double c[3] = { get<0>(in)[i + 2], get<1>(in)[i + 2], get<2>(in)[i + 2] };
        //ベクトル化
        long double v1[3] = { get<0>(in)[i + 1] - get<0>(in)[i], get<1>(in)[i + 1] - get<1>(in)[i], get<0>(in)[i + 1] - get<2>(in)[i] };
        long double v2[3] = { get<0>(in)[i + 2] - get<0>(in)[i], get<1>(in)[i + 2] - get<1>(in)[i], get<0>(in)[i + 2] - get<2>(in)[i] };
        //外積演算
        long double gaiseki[3] = {
            v1[1] * v2[2] - v1[2] * v2[1],
            v1[2] * v2[0] - v1[0] * v2[2],
            v1[0] * v2[1] - v1[1] * v2[0]
        };
        //外積の大きさ
        long double length = std::sqrt(
            gaiseki[0] * gaiseki[0]
            + gaiseki[1] * gaiseki[1]
            + gaiseki[2] * gaiseki[2]
        );
        //外積の正規化
        long double faceVertical[3] = { gaiseki[0] / length, gaiseki[1] / length, gaiseki[2] / length };


        /*cout << "gaiseki[0]=" << gaiseki[0] << '\n';
        cout << "gaiseki[1]=" << gaiseki[1] << '\n';
        cout << "gaiseki[2]=" << gaiseki[2] << '\n';
        cout << "length=" << length << '\n';
        cout << "faceVertical[0]=" << faceVertical[0] << '\n';
        cout << "faceVertical[1]=" << faceVertical[1] << '\n';
        cout << "faceVertical[2]=" << faceVertical[2] << '\n';
        */
        //cout << "x[k]=" << fixed << setprecision(12) << x[k] << '\n';

        //テキストに書き込み(2行目以降)
        //2行目
        outputfile
            << " facet normal "
            << std::fixed << std::setprecision(12) << faceVertical[0] << ' ' << faceVertical[1] << ' ' << faceVertical[2] << endl;
        //3行目
        outputfile
            << " outer loop" << endl
            << " vertex " << get<0>(in)[i] << ' ' << get<1>(in)[i] << ' ' << get<2>(in)[i] << endl
            << " vertex " << get<0>(in)[i+1] << ' ' << get<1>(in)[i+1] << ' ' << get<2>(in)[i+1] << endl
            << " vertex " << get<0>(in)[i+2] << ' ' << get<1>(in)[i+2] << ' ' << get<2>(in)[i+2] << endl
            << " endloop" << endl
            << " endfacet" << endl;
    }
    //テキストに書き込み(最終行) & 書き込みの終了処理
    outputfile << R"(endsolid "TextForSTL")" << endl;
    getchar();
    return 0;
}