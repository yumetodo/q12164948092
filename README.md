# How to Download

```sh
git clone https://yumetodo@bitbucket.org/yumetodo/q12164948092.git
```

# How to build

```sh
cmake .
make
```

or

```sh
g++ -std=c++11 -O0 -g -Wall -Wextra main.cpp
```

or

```sh
clang++ -std=c++11 -O2 -Wall -Wextra main.cpp
```

or

```bat
cl.exe /EHsc main.cpp
```

# How to run

```sh
./a.exe
```

or

```sh
./a.out
```

# How to make Bool1.csv semi-automatically

http://melpon.org/wandbox/permlink/9r8i8PXAYPAGSBBo

# ref

http://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q12164948092
